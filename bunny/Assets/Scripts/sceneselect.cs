﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneselect : MonoBehaviour
{
    public void selectScene()
    {
        switch (this.gameObject.name)
        {
            case "Level 1 button":
                SceneManager.LoadScene("Level 1");
                break;
            case "Level 2 button":
                SceneManager.LoadScene("Level 2");
                break;
            case "Level 3 button":
                SceneManager.LoadScene("Level 3");
                break;
            case "Tutorial button":
                SceneManager.LoadScene("Tutorial");
                break;
        }
    }
}
