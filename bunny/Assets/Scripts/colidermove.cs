﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class colidermove : MonoBehaviour
{
    public Rigidbody2D rb;
    public bool play = false;
    // Start is called before the first frame update
    void Start()
    {

    }
    //empty void =  bunny start button benutzen
    public void BunnyRunStart()
    {
        play = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (play == true)
        {
            rb.velocity = transform.right * 2f;
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("getroffen1");


        if (col.tag == "Pfeil")
        {
            Debug.Log("getroffen");
            //hase wird auf die Position des kollidierenden Objektes gesetzt
            this.gameObject.transform.position = col.transform.position;

            //hase wird in richtung des Pfeiles gedreht
            this.gameObject.transform.rotation = col.transform.rotation;

           
        }


        if (col.tag == "Ziel")
        {
            SceneManager.LoadScene("Menu");
        }

        if (col.tag == "Water")
                {
            Debug.Log("tot");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
        
            }
        

    }
